package exercise;

class Converter {
    // BEGIN
    public static void main(String[] args) {
        int bytesInKilobytes = Converter.convert(10, "b");
        System.out.println("10 Kb = " + bytesInKilobytes + " b");
    }

    public static int convert(int number, String unit) {
        switch (unit) {
            case "Kb":
                return number / 1024;
            case "b":
                return number * 1024;
            default:
                return 0;
        }
        // END
    }
}

