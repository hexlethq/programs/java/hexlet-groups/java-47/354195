package exercise;

class Triangle {
    // BEGIN
    public static void main(String[] args) {
        double square = getSquare(4, 5, 45);
        System.out.println(square);
    }

    public static double getSquare(double a, double b, double angle) {
        double toRadian =  angle * Math.PI / 180;
        double sin = Math.sin(toRadian);
        return ((a * b) / 2) * sin;
    }
    // END
}
