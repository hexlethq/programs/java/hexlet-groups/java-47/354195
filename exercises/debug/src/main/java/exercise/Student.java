package exercise;

public class Student {

    public static int getFinalGrade(int exam, int project) {
        if (exam > 90 || project > 10) {
            return 100;
        } else if (exam > 75 && project >= 5) {
            return 90;
        } else if (exam > 50 && project >= 2) {
            return 75;
        } else {
            return 0;
        }
    }

    public static void main(String[] args) {
        System.out.println(Student.getFinalGrade(100, 12));
        System.out.println(Student.getFinalGrade(99, 0));
        System.out.println(Student.getFinalGrade(10, 15));
        System.out.println(Student.getFinalGrade(85, 5));
        System.out.println(Student.getFinalGrade(55, 3));
        System.out.println(Student.getFinalGrade(55, 0));
    }
}
