package exercise;

class App {
    // BEGIN
    public static String getTypeOfTriangle(double a, double b, double c) {
        if (a + b <= c || a + c <= b || b + c <= a) {
            return "Треугольник не существует";
        }
        if (a == b && b == c) {
            return "Равносторонний";
        }
        if (a == b || b == c || a == c) {
            return "Равнобедренный";
        }
        return "Разносторонний";
    }
    // END
}
