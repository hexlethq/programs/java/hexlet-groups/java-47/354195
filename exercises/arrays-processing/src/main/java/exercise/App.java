package exercise;

import java.util.Arrays;

class App {
    // BEGIN
    public static int getIndexOfMaxNegative(int[] input) {
        if (input == null || input.length == 0) {
            return -1;
        }

        int minValueIndex = 0;

        for (int i = 1; i < input.length; i++) {
            int current = input[i];
            if (current < 0 && current > input[minValueIndex]) {
                minValueIndex = i;
            }
        }

        if (input[minValueIndex] >= 0) {
            return -1;
        }

        return minValueIndex;
    }

    public static int[] getElementsLessAverage(int[] input) {
        if (input.length == 0)
            return new int[0];

        int sum = 0;
        for (int elem : input) {
            sum += elem;
        }

        int index = 0;
        double average = sum / input.length;
        int[] result = new int[input.length];
        for (int num : input) {
            if (num <= average) {
                result[index] = num;
                index += 1;
            }
        }
        result = Arrays.copyOfRange(result, 0, index);
        return result;
    }

    public static int getSumBeforeMinAndMax(int[] input) {

        int min = input[0];
        int max = input[0];
        int indexOfMin = 0;
        int indexOfMax = 0;

        for (int i = 1; i < input.length; i++) {
            if (input[i] < min) {
                min = input[i];
                indexOfMin = i;
            }
            if (input[i] > max) {
                max = input[i];
                indexOfMax = i;
            }
        }

        if (indexOfMin > indexOfMax) {
            int temp = indexOfMax;
            indexOfMax = indexOfMin;
            indexOfMin = temp;
        }

        int sum = 0;
        for (int j = indexOfMin + 1; j < indexOfMax; j++) {
            sum += input[j];
        }

        return sum;
    }
    // END
}

