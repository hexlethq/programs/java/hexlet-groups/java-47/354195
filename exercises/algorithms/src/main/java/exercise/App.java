package exercise;

class App {
    // BEGIN
    public static int[] sort(int[] input) {
        for (int i = 0; i < input.length - 1; i++) {
            for (int j = input.length - 1; j > i; j--) {
                if (input[j - 1] > input[j]) {
                    int temp = input[j - 1];
                    input[j - 1] = input[j];
                    input[j] = temp;
                }
            }
        }
        return input;
    }

    public static int[] selectionSort(int[] input) {
        for (int i = 0; i < input.length; i++) {
            int least = i;
            int min = input[i];

            for (int j = i + 1; j < input.length; j++) {
                if (input[j] < min) {
                    least = j;
                    min = input[j];
                }
            }
            input[least] = input[i];
            input[i] = min;
        }
        return input;
    }
    // END
}
