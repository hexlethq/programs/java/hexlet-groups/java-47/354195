package exercise;

import java.sql.SQLOutput;

class App {
    public static boolean isBigOdd(int number) {
        // BEGIN
        boolean isBigOddVariable = number % 2 == 1 && number > 1000;
        return isBigOddVariable;
        // END
    }

    public static void sayEvenOrNot(int number) {
        // BEGIN
        String result = (number % 2 == 0) ?  "yes" : "no";
        System.out.println(result);
        // END
    }

    public static void printPartOfHour(int minutes) {
        // BEGIN
        String partOfHour;
        if (minutes >= 0 && minutes < 15) {
            partOfHour = "First";
        } else if (minutes >= 15 && minutes <= 30) {
            partOfHour = "Second";
        } else if (minutes >= 31 && minutes <= 45) {
            partOfHour = "Third";
        } else if (minutes >= 46 && minutes <= 59) {
            partOfHour = "Fourth";
        } else {
            partOfHour = "Error! Please enter the number from 0 to 59";
        }
        System.out.println(partOfHour);
        // END
    }
}
