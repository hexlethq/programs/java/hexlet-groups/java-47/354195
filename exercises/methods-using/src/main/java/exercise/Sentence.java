package exercise;


import java.sql.SQLOutput;

class Sentence {
    public static void printSentence(String sentence) {
        // BEGIN
        String result;
        if (sentence.endsWith("!")) {
            result = sentence.toUpperCase();
        } else {
            result = sentence.toLowerCase();
        }
        System.out.println(result);
        // END
    }
}
