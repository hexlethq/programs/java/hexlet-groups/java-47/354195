package exercise;

class App {
    // BEGIN
    public static String getAbbreviation(String phrase) {
        String result = "";

        for (int i = 0; i < phrase.length(); i++) {
            char current = phrase.charAt(i);
            char prev = i == 0 ? ' ' : phrase.charAt(i - 1);
            final boolean currentCharIsNotSpace = current != ' ';
            final boolean prevCharIsSpace = prev == ' ';

            if (currentCharIsNotSpace && prevCharIsSpace) {
                result = result + Character.toUpperCase(current);
            }
        }
        return result;
    }
    // END
}
