package exercise;

class App {
    // BEGIN
    public static int[] reverse(int[] array) {
        int[] newArray = new int[array.length];

        for (int i = 0; i < array.length; i++) {
            newArray[array.length - 1 - i] = array[i];
        }
        return newArray;
    }

    public static int mult(int[] array) {
        int result = 1;
        for (int num : array) {
            result = result * num;
        }
        return result;
    }

    public static int[] flattenMatrix(int[][] matrix) {
        if (matrix.length == 0)
            return new int[0];

        int[] flattenArray = new int[matrix.length * matrix[0].length];
        int index = 0;
        for (int[] row : matrix) {
            for (int elem : row) {
                flattenArray[index] = elem;
                index += 1;
            }
        }
        return flattenArray;
    }
    // END
}
